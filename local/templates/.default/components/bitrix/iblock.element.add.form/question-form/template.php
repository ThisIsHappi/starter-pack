<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>

<form class="form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<?=bitrix_sessid_post()?>
    <strong class="form__title"><?=$arParams["FORM_TITLE"]?></strong>
	<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
        <?if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "FORM_TEXTAREA"): ?>
            <textarea name="PROPERTY[<?=$propertyID?>][0]" cols="30" rows="4" class="form__input form__textarea" placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>"></textarea>
        <?elseif ($propertyID == "NAME"): ?>
            <input type="hidden" name="PROPERTY[NAME][]" value="Вопрос - <?=date("d.m.Y H:m:s")?>">
        <?else:?>
            <input type="text" class="form__input" name="PROPERTY[<?=$propertyID?>][0]" placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>">
        <?endif; ?>
	<?endforeach;?>
    <input class="form__button button button_large" type="submit" name="iblock_submit" value="<?=$arParams["BUTTON_TEXT"]?>" />
</form>
