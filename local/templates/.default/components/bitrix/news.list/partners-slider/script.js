$(document).ready(function(){
    var partners = new Swiper('.partners-slider', {
        slidesPerView: 4,
        calculateHeight:true,
        nextButton: '.partners-slider-next',
        prevButton: '.partners-slider-prev',
        simulateTouch: false,
        breakpoints: {
            1366: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 1
            }
        }
    });
});
