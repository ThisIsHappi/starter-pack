var stylesArray = [
    {
        stylers: [
            {saturation: -100}
        ]
    }
]

function initMap() {
    var geocoder = new google.maps.Geocoder();
    var thisAddress;
    $.each($('.contacts-item__map'),function(i, val){
        var map = new google.maps.Map(val, {
            zoom: 15
        });
        var thisAddress = $(this).data('address');
        geocodeAddress(geocoder, map, thisAddress);
        map.setOptions({styles: stylesArray});
    });
}

function geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location,
                icon: '/local/templates/.default/components/bitrix/news.list/contacts-list/images/marker.png'
            });
        }
    });
}

$(document).ready(function(){
    initMap();
});
