<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANADmK1hCPc99hN5DjYSjI0jmead-9TCc&libraries=places"></script>
<div class="sidebar-contacts">
    <strong class="sidebar-contacts__title"><?=$arParams["FORM_TITLE"]?></strong>
    <a href='mailto:<?=COption::GetOptionString("main", "email_from");?>' class="sidebar-contacts__mail"><i class="sidebar-contacts__icon icon icon_mail"></i><?=COption::GetOptionString("main", "email_from");?></a>
    <?foreach($arResult["ITEMS"] as $arItem):?>
    	<?
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    	?>
        <div class="contacts-item">
            <span class="contacts-item__city"><?=$arItem["PROPERTIES"]["CONTACTS_CITY"]["VALUE"]?></span>
            <strong class="contacts-item__tel"><?=$arItem["PROPERTIES"]["CONTACTS_PHONE"]["VALUE"]?></strong>
            <div class="contacts-item__map" data-address="<?=$arItem["PROPERTIES"]["CONTACTS_ADDRESS"]["VALUE"]?>"></div>
            <address class="contacts-item__address">
                <i class="contacts-item__icon icon icon_marker"></i>
                <?=$arItem["PROPERTIES"]["CONTACTS_ADDRESS"]["VALUE"]?>
            </address>
        </div>
    <?endforeach;?>
</div>
