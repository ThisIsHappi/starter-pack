<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <?if ($arParams["NEWS_TITLE"] !== ""): ?>
        <strong class="title"><?=$arParams["NEWS_TITLE"]?></strong>
    <?endif; ?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
    	<?
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    	?>
        <div class="news-list__item">
            <a href="" class="news-list__link"><?=$arItem["PROPERTIES"]["NEWS_TITLE"]["VALUE"]?></a>
            <p class="news-list__descr"><?=$arItem["PROPERTIES"]["NEWS_DESCR"]["VALUE"]["TEXT"]?></p>
            <div class="news-list__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
            <?if ($arItem["DISPLAY_PROPERTIES"]["NEWS_AUTHOR"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["NEWS_AUTHOR"]["VALUE"]]["DETAIL_PICTURE"]): ?>
                <img src="<?=CFile::GetPath($arItem["DISPLAY_PROPERTIES"]["NEWS_AUTHOR"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["NEWS_AUTHOR"]["VALUE"]]["DETAIL_PICTURE"])?>" alt="" class="news-list__author-logo">
            <?endif; ?>
        </div>
    <?endforeach;?>
    <?if (count($arResult["ITEMS"]) >= $arParams["NEWS_COUNT"]): ?>
        <a href="/" class="button button_reverse button_caret"><?=$arParams["ALL_NEWS_BUTTON"]?><i class="icon icon_button-arrow"></i></a>
    <?endif; ?>
</div>
