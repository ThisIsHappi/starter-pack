$(document).ready(function(){
    var mySwiper = new Swiper('.main-slider', {
        pagination: '.main-slider__pagination',
        paginationClickable: true,
        simulateTouch: false
    });
});
