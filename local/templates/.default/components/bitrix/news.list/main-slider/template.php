<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main-slider swiper-container">
    <div class="swiper-wrapper">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    	<?
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    	?>
        <div class="swiper-slide">
            <img src="<?=$arItem["DISPLAY_PROPERTIES"]["SLIDER_BACKGROUND"]["FILE_VALUE"]["SRC"]?>" alt="" class="main-slider__bg">
            <div class="main-slider__content">
                <strong class="main-slider__title"><?=$arItem["PROPERTIES"]["SLIDER_TITLE"]["VALUE"]?></strong>
                <p class="main-slider__descr"><?=$arItem["PROPERTIES"]["SLIDER_DESCR"]["VALUE"]?></p>
            </div>
        </div>
    <?endforeach;?>
    </div>
    <?if (count($arResult["ITEMS"]) > 1): ?>
        <div class="main-slider__pagination swiper-pagination"></div>
    <?endif; ?>
</div>
