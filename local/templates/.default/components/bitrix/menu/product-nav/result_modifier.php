<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");

$arResultTemp = $arResult;
$indexPosition = 1;

foreach ($arResult as $index => $menuItem) {
    $arrListID = $menuItem["PARAMS"]["GET_LIST_ID"];
    if(!empty($arrListID) && is_array($arrListID)){
        $res = CIBlock::GetList(
            Array(),
            Array(
                'ACTIVE'     => "Y",
                "CNT_ACTIVE" => "Y",
                "ID"         => $arrListID
            ),
            true
        );

        $item_index_dl1 = 0;
        if($res->SelectedRowsCount() > 0 ){
            $arResultTemp[$indexPosition - 1]["IS_PARENT"] = 1;
        }
        while($ar_res = $res->Fetch()){
            if($ar_res['ELEMENT_CNT'] > 0){
                $arItemTemp = array(
                    "TEXT" => $ar_res['NAME'],
                    "ITEM_INDEX" => $item_index_dl1++,
                    "DEPTH_LEVEL" => 2,
                    "IS_PARENT" => false
                );
                array_splice($arResultTemp, $indexPosition++, 0, array($arItemTemp));

                /*получаем елементы*/
                $arItemTemp = array();
                $arSelect = Array("ID", "NAME", "DETAIL_URL");
                $arFilter = Array("IBLOCK_ID" => $ar_res['ID'], "ACTIVE"=>"Y");
                $res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($res1->SelectedRowsCount() > 0 ){
                    $arResultTemp[$indexPosition - 1]["IS_PARENT"] = 1;
                }
                $item_index_dl2 = 0;
                while($ob = $res1->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $arItemTemp[] = array(
                        "TEXT" => $arFields['NAME'],
                        "LINK" => $arFields['DETAIL_URL'],
                        "ITEM_INDEX" => $item_index_dl2++,
                        "DEPTH_LEVEL" => 3,
                        "IS_PARENT" => false
                    );
                }
                if(!empty($arItemTemp)){
                    array_splice($arResultTemp, $indexPosition, 0, $arItemTemp);
                    $indexPosition += count($arItemTemp);
                }
            }
        }
    }
    $indexPosition++;
}

$arResult = $arResultTemp;

?>
