<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <ul class="aside-nav depth-level-0">
        <?
        $previousLevel = 0;
        foreach($arResult as $arItem):?>
        	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
        		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
        	<?endif?>
            <li class="aside-nav__item">
                <a href="<?=$arItem["LINK"]?>" class="aside-nav__link <? if ($arItem["PARAMS"]["TITLE_ITEM"] == "Y"):?>aside-nav__link_main<?endif;?>"><?=$arItem["TEXT"]?></a>
        	<?if ($arItem["IS_PARENT"]):?>
                <ul class="aside-nav aside-nav_dropdown depth-level-<?=$arItem["DEPTH_LEVEL"]?>">
            <?endif?>
        	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
        <?endforeach?>
        <?if ($previousLevel > 1):?>
        	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
        <?endif?>
    </ul>
<?endif?>
