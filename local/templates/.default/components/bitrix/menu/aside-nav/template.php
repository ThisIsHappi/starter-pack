<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<!-- <?print_r($arResult);?> -->
<ul class="aside-nav">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="aside-nav__item"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>aside-nav__link aside-nav__link_selected<?else:?>aside-nav__link<?endif?>"><?=$arItem["TEXT"]?></a>
				<ul class="aside-nav_dropdown">
		<?else:?>
			<li class="aside-nav__item"><a href="<?=$arItem["LINK"]?>" class="parent<?if ($arItem["SELECTED"]):?> item-selected<?endif?>"><?=$arItem["TEXT"]?></a>
				<ul class="aside-nav_dropdown">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="aside-nav__item"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>aside-nav__link aside-nav__link_selected<?else:?>aside-nav__link<?endif?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li class="aside-nav__item"><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="aside-nav__item"><a href="" class="<?if ($arItem["SELECTED"]):?>aside-nav__link aside-nav__link_selected<?else:?>aside-nav__link<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li class="aside-nav__item"><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?endif?>
