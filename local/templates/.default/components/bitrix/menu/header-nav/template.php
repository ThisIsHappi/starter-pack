<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <ul class="header-nav">
        <?
        $previousLevel = 0;
        foreach($arResult as $arItem):?>
        	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
        		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
        	<?endif?>
            <li class="header-nav__item <?if ($arItem["PARAMS"]["LOGO"] == "Y"):?>header-nav__item_logo<?endif?>">
                <a href="<?=$arItem["LINK"]?>" class="header-nav__link <?if ($arItem["SELECTED"]):?>header-nav__link_active<?else:?>root-item<?endif?>">
                    <img src="<?=$templateFolder?>/images/header-nav__<?=$arItem["PARAMS"]["ICON_TYPE"]?>.png" class="header-nav__icon">
                    <span class="header-nav__text"><?=$arItem["TEXT"]?></span>
                </a>
        	<?if ($arItem["IS_PARENT"]):?>
                <ul class="header-nav_dropdown depth-level-<?=$arItem["DEPTH_LEVEL"]?>">
            <?endif?>
        	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
        <?endforeach?>
        <?if ($previousLevel > 1):?>
        	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
        <?endif?>
    </ul>
<?endif?>
