<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="filter">
    <form class="" name="<?= $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
        <input type="hidden" name='AJAXSELECTION_IBLOCK_ID' value='<?=$arResult["IBLOCK_ID"];?>' />
        <input type="hidden" name='AJAXSELECTION_SECTION_CODE' value='<?=$arResult["SECTION_CODE"];?>' />
        <?foreach($arResult["ITEMS"] as $arItem):
    		if(array_key_exists("HIDDEN", $arItem)):
    			echo $arItem["INPUT"];
    		endif;
    	endforeach;?>

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if($arItem["TYPE"] == "CHECKBOX"):?>
                <div class="filter__item">
                    <strong class="filter__title"><?=$arItem["NAME"]?></strong>
                    <?foreach ($arItem["LIST"] as $arrInput): ?>
                        <div class="checkbox">
                            <input type="checkbox" name="<?=$arItem["INPUT_NAME"]?>[<?=$arrInput["ID"]?>]" id="input_<?=$arrInput["ID"]?>" class="checkbox__input" <?if (array_key_exists($arrInput["ID"], $_REQUEST["productFilter_ff"][$arrInput["IBLOCK_CODE"]])):?>checked<?endif;?>>
                            <label for="input_<?=$arrInput["ID"]?>" class="checkbox__label"><?=$arrInput["NAME"]?></label>
                        </div>
                    <?endforeach; ?>
                </div>
            <?else:?>
                <?if(!array_key_exists("HIDDEN", $arItem)):?>
                    <div class="filter-search">
                        <strong class="filter__title">Поиск по названию</strong>
                        <div class="search-form">
                            <input type="text" name="<?=$arItem["INPUT_NAME"]?>" class="search-form__input" placeholder="Введите название" value="<?if ($_REQUEST["productFilter_ff"]["NAME"]):?><?=$_REQUEST["productFilter_ff"]["NAME"]?><?endif;?>">
                            <span class="search-form__submit"></span>
                        </div>
                    </div>
                <?endif?>
            <?endif?>
        <?endforeach;?>
        <input type="submit" name="set_filter" class="filter__button button" value="Применить" />
    </form>
</div>
