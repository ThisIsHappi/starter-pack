$(document).ready(function(){
    var offsetTop = $('.product').offset().top;
    $('form[name="productFilter_form"]').submit(function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        formData += '&set_filter=Применить';
        $.ajax({
            url: "/include/ajax/ajaxform.php",
            type: 'POST',
            data: formData,
            dataType: 'html',
            beforeSend: function() {
                $('.preloader-overlay').addClass('js-active');
                $('body').stop().animate({scrollTop:offsetTop});
            },
            success: function(result) {
                $('.product-list').html(result);
            },
            complete: function() {
                $('.preloader-overlay').removeClass('js-active');
            }
        });
    });
});
