<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['IBLOCK_ID'] = $arParams["IBLOCK_ID"];
$arResult['SECTION_CODE'] = $arParams["SECTION_CODE"];

$cntIBLOCK_List = 10;
$cache = new CPHPCache();
$cache_time = 3600;
$cache_id = 'arIBlockListID'.$cntIBLOCK_List;
$cache_path = '/arIBlockListID/';
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
    $res = $cache->GetVars();
    if (is_array($res["arIBlockListID"]) && (count($res["arIBlockListID"]) > 0))
        $arIBlockListID = $res["arIBlockListID"];
}
if (!is_array($arIBlockListID)) {
    foreach ($arResult["arrProp"] as $key => $propItem) {
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_CODE");
        $arFilter = Array("IBLOCK_ID"=>$propItem["LINK_IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        while($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arResult["ITEMS"]["PROPERTY_".$key]["LIST"][] = $arFields;
        }
    }
    //////////// end cache /////////
    if ($cache_time > 0) {
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        $cache->EndDataCache(array("arIBlockListID"=>$arIBlockListID));
    }
}
?>
