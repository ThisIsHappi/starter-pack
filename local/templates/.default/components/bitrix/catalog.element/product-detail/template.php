<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="sidebar sidebar_right">
    <?$APPLICATION->IncludeComponent("bitrix:news.list","news",Array(
            "NEWS_TITLE" => "Смотрите также",
            "ALL_NEWS_BUTTON" => "Все новости",
            "IBLOCK_TYPE" => "",
            "IBLOCK_ID" => "4",
            "NEWS_COUNT" => "2",
            "SORT_BY1" => "ID",
            "SORT_ORDER1" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => Array("ID"),
            "PROPERTY_CODE" => Array("NEWS_AUTHOR"),
            "CHECK_DATES" => "N",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d F Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "PARENT_SECTION" => "1",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N"
        )
    );?>
</div>
<div class="product-detail">
    <strong class="product-detail__title"><?=$arResult["NAME"]?></strong>
    <div class="product-detail__banner">
        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" class="product-detail__img">
        <span class="product-detail__code"><?=$arResult["PROPERTIES"]["PRODUCT_CODE"]["~VALUE"]["TEXT"]?></span>
    </div>
    <p class="text">
        <b class="text text_marked">Отрасль применения:</b>
        <?=implode(", ", $arResult["PROPERTIES"]["PRODUCT_SECTOR_NAME"]);?>
    </p>
    <svg class="divider">
        <line x1="4" x2="100%" y1="7" y2="7" stroke="#1076bc" stroke-width="7" stroke-linecap="round" stroke-dasharray="0.001, 50"></line>
    </svg>
    <p class="text"><?=$arResult["DETAIL_TEXT"]?></p>
    <a href="" class="product-detail__button button">Заказать</a>
    <?if ($arResult["PROPERTIES"]["PRODUCT_DOCS"]["VALUE"]): ?>
        <?$path = CFile::GetPath($arResult["PROPERTIES"]["PRODUCT_DOCS"]["VALUE"]);?>
        <a href="<?=$path?>" class="product-detail__button button">Скачать спецификацию</a>
    <?endif; ?>
    <ul class="extras">
        <li class="extras__item"><a href="" class="extras__link extras__link_share"></a></li>
        <li class="extras__item"><a href="" class="extras__link extras__link_send"></a></li>
        <li class="extras__item"><a href="" class="extras__link extras__link_print"></a></li>
    </ul>
    <!-- <p class="text">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
    <h3 class="content-title content-title_small">Lorem ipsum dolor sit amet</h3>
    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
    <h4 class="content-title content-title_subtitle">Lorem ipsum dolor sit amet</h4>
    <ul class="list">
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
    </ul>
    <h4 class="content-title content-title_subtitle">Lorem ipsum dolor sit amet</h4>
    <ul class="list list_numbered">
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
        <li class="list__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
    </ul>
    <h4 class="content-title content-title_subtitle">Lorem ipsum dolor sit amet</h4> -->
    <!-- SLIDEDOWN -->

    <!-- SLIDEDOWN -->
</div>
