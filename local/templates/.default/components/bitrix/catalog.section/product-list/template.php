<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="preloader-overlay">
    <img src="/images/icons/preloader.svg" alt="" class="preloader product__preloader">
</div>
<?if ($arParams["AJAX_FILTER"] !== "Y"): ?>
    <div class="product">
        <strong class="title product__title"><?=$arParams["TITLE"]?></strong>
        <ul class="product-list">
<?endif; ?>

<?foreach ($arResult["ITEMS"] as $arItem):?>
    <li class="product-list__item">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="product-list__link">
            <img src="/images/product-list__img_2.png" alt="" class="product-list__img"><?=$arItem["NAME"]?>
        </a>
    </li>
<?endforeach; ?>

<?if ($arParams["AJAX_FILTER"] !== "Y"): ?>
        </ul>
        <?if ($arParams["DISPLAY_BOTTOM_PAGER"])
        {
            ?><? echo $arResult["NAV_STRING"]; ?><?
        }?>
    </div>
<?endif; ?>
