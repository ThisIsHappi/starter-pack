<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
            </main>
            <footer class="footer">
                <ul class="footer-nav">
                    <li class="copyright footer-nav__item">© 2016 ООО «Пермсоль»</li>
                    <li class="footer-nav__item"><a href="/" class="footer-nav__link">Карта сайта</a></li>
                    <li class="footer-nav__item"><a href="/contacts/" class="footer-nav__link">Контакты</a></li>
                </ul>
                <ul class="social">
                    <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_vk"></i></a></li>
                    <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_od"></i></a></li>
                    <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_fb"></i></a></li>
                    <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_gg"></i></a></li>
                </ul>
            </footer>
        </div>
    </div>
</body>
</html>
