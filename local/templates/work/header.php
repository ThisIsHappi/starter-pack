<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
<link rel="stylesheet" href="/js/plugins/swiper/swiper.min.css">
<script src="/js/plugins/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="/css/main.css">

<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=cyrillic" rel="stylesheet">
<script src="/js/plugins/swiper/swiper.min.js"></script>
<script src="/js/main.js"></script>
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<div class="wrap clearfix">
    <aside class="sidebar">
        <div class="logo">
            <a href="/" class="logo__link"><img src="/images/logo.png" alt="" class="logo__img"></a>
        </div>
        <?
            if ($APPLICATION->GetCurPage(false) == "/products/") {
                $navTemplate = "product-nav";
            } else {
                $navTemplate = "aside-nav";
            }
        ?>
        <?$APPLICATION->IncludeComponent("bitrix:menu",$navTemplate,Array(
                "ROOT_MENU_TYPE" => "aside",
                "MAX_LEVEL" => "4",
                "CHILD_MENU_TYPE" => "aside-child",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "Y",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "0",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => ""
            )
        );?>
    </aside>
    <div class="content">
        <header class="header">
            <div class="header-top">
                <?$APPLICATION->IncludeComponent("bitrix:search.form","search-form",Array(
                    "USE_SUGGEST" => "N",
                    "PAGE" => "#SITE_DIR#search/index.php"
                    )
                );?>
                <a href="/en/" class="lang-button">EN</a>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu","header-nav",Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "child",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "0",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </header>
        <main class="main-content">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumbs",Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
                )
            );?>
