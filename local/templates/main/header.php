<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
<link rel="stylesheet" href="/js/plugins/swiper/swiper.min.css">
<script src="/js/plugins/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="/css/main.css">

<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=cyrillic" rel="stylesheet">
<script src="/js/plugins/swiper/swiper.min.js"></script>
<script src="/js/main.js"></script>
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<div class="wrap clearfix">
    <aside class="sidebar">
        <div class="logo">
            <a href="/" class="logo__link"><img src="/images/logo.png" alt="" class="logo__img"></a>
        </div>
        <ul class="social">
            <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_vk"></i></a></li>
            <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_od"></i></a></li>
            <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_fb"></i></a></li>
            <li class="social__item"><a href="" class="social__link"><i class="social__icon icon icon_gg"></i></a></li>
        </ul>
        <?$APPLICATION->IncludeComponent("bitrix:news.list","contacts-list",Array(
                "FORM_TITLE" => "Свяжитесь с нами",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "Y",
                "IBLOCK_TYPE" => "news",
                "IBLOCK_ID" => "1",
                "NEWS_COUNT" => "2",
                "SORT_BY1" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => Array("ID"),
                "PROPERTY_CODE" => Array("DESCRIPTION"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "Y",
                "SET_BROWSER_TITLE" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_META_DESCRIPTION" => "Y",
                "SET_LAST_MODIFIED" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "Y",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "Y",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SET_STATUS_404" => "Y",
                "SHOW_404" => "Y",
                "MESSAGE_404" => "",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            )
        );?>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:iblock.element.add.form",
        	"question-form",
        	array(
        		"FORM_TITLE" => "ЗАДАЙТЕ НАМ ВОПРОС",
        		"BUTTON_TEXT" => "Отправить",
        		"SEF_MODE" => "Y",
        		"IBLOCK_TYPE" => "Forms",
        		"IBLOCK_ID" => "2",
        		"PROPERTY_CODES" => array(
        			0 => "6",
        			1 => "7",
        			2 => "8",
        			3 => "NAME",
        		),
        		"PROPERTY_CODES_REQUIRED" => array(
        		),
        		"LIST_URL" => "",
        		"ELEMENT_ASSOC" => "PROPERTY_ID",
        		"ELEMENT_ASSOC_PROPERTY" => "",
        		"MAX_USER_ENTRIES" => "100000",
        		"MAX_LEVELS" => "100000",
        		"LEVEL_LAST" => "Y",
        		"USE_CAPTCHA" => "N",
        		"USER_MESSAGE_EDIT" => "",
        		"USER_MESSAGE_ADD" => "",
        		"DEFAULT_INPUT_SIZE" => "30",
        		"RESIZE_IMAGES" => "Y",
        		"MAX_FILE_SIZE" => "0",
        		"PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
        		"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
        		"SEF_FOLDER" => "/",
        		"COMPONENT_TEMPLATE" => "question-form",
        		"STATUS_NEW" => "N",
        		"GROUPS" => array(
        			0 => "2",
        		),
        		"STATUS" => "ANY",
        		"CUSTOM_TITLE_NAME" => "",
        		"CUSTOM_TITLE_TAGS" => "",
        		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
        		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
        		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
        		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
        		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
        		"CUSTOM_TITLE_DETAIL_TEXT" => "",
        		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
        	),
        	false
        );?>
    </aside>
    <div class="content">
        <header class="header">
            <div class="header-top">
                <?$APPLICATION->IncludeComponent("bitrix:search.form","search-form",Array(
                    "USE_SUGGEST" => "N",
                    "PAGE" => "#SITE_DIR#search/index.php"
                    )
                );?>
                <a href="/en/" class="lang-button">EN</a>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu","header-nav",Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "child",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "0",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </header>
        <main class="main-content">
